<?php 
    function retornaFecha($lang='val'){
        $mes=date(n);
        $diaSem=date(N);
        $diaMes=date(j);
        $anyo=date(Y);
        if(strtoupper($lang)=='ES'){
            switch($diaSem){
                case 1;
                    $diaSemFinal='Lunes';
                    break;
                case 2;
                    $diaSemFinal='Martes';
                    break;
                case 3;
                    $diaSemFinal='Miercoles';
                    break;
                case 4;
                    $diaSemFinal='Jueves';
                    break;
                case 5;
                    $diaSemFinal='Viernes';
                    break;
                case 6;
                    $diaSemFinal='Sabado';
                    break;
                case 7;
                    $diaSemFinal='Domingo';
                    break;
            }
            switch($mes){
                case 1;
                    $mesFinal='Enero';
                    break;
                case 2;
                    $mesFinal='Febrero';
                    break;
                case 3;
                    $mesFinal='Marzo';
                    break;
                case 4;
                    $mesFinal='Abril';
                    break;
                case 5;
                    $mesFinal='Mayo';
                    break;
                case 6;
                    $mesFinal='Junio';
                    break;
                case 7;
                    $mesFinal='Julio';
                    break;
                case 8;
                    $mesFinal='Agosto';
                    break;
                case 9;
                    $mesFinal='Septiembre';
                    break;
                case 10;
                    $mesFinal='Octubre';
                    break;
                case 11;
                    $mesFinal='Noviembre';
                    break;
                case 12;
                    $mesFinal='diciembre';
                    break;
            }
        }else{
            switch($diaSem){
                case 1;
                    $diaSemFinal='Dilluns';
                    break;
                case 2;
                    $diaSemFinal='Dimarts';
                    break;
                case 3;
                    $diaSemFinal='Dimecres';
                    break;
                case 4;
                    $diaSemFinal='Dijous';
                    break;
                case 5;
                    $diaSemFinal='Divendres';
                    break;
                case 6;
                    $diaSemFinal='Disabte';
                    break;
                case 7;
                    $diaSemFinal='Diumenge';
                    break;
            }
            switch($mes){
                case 1;
                    $mesFinal='Gener';
                    break;
                case 2;
                    $mesFinal='Febrer';
                    break;
                case 3;
                    $mesFinal='Març';
                    break;
                case 4;
                    $mesFinal='Abril';
                    break;
                case 5;
                    $mesFinal='Maig';
                    break;
                case 6;
                    $mesFinal='Juny';
                    break;
                case 7;
                    $mesFinal='Juliol';
                    break;
                case 8;
                    $mesFinal='Agost';
                    break;
                case 9;
                    $mesFinal='Setembre';
                    break;
                case 10;
                    $mesFinal='Octubre';
                    break;
                case 11;
                    $mesFinal='Novembre';
                    break;
                case 12;
                    $mesFinal='Decembre';
                    break;
        }
    }
    return "$diaSemFinal, $diaMes de $mesFinal de $anyo ";
}    
    
?>