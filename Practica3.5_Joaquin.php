<?php
    function opera(callable $operacion,$oper1,$oper2){
        $operacion($oper1,$oper2);
    }
    $suma=function($oper1,$oper2){
        $resultado=$oper1+$oper2;
        echo 'La suma de '.$oper1.' + '.$oper2.' es = '.$resultado.'</br></br>';
    };

    $resta=function($oper1,$oper2){
        $resultado=$oper1-$oper2;
        echo 'La resta de '.$oper1.' - '.$oper2.' es = '.$resultado.'</br></br>';
    };

    $multiplica=function($oper1,$oper2){
        $resultado=$oper1*$oper2;
        echo 'La multiplicacion de '.$oper1.' * '.$oper2.' es = '.$resultado.'</br></br>';
    };

    $divide=function($oper1,$oper2){
        $resultado=$oper1/$oper2;
        echo 'La division de '.$oper1.' / '.$oper2.' es = '.$resultado.'</br></br>';
    };
    opera($suma,3,4);
    opera($resta,8,4);

    opera($multiplica,8,4);
    opera($divide,8,4);

?>