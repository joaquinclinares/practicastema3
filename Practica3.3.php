<?php
    $enter= intval(htmlspecialchars($_GET['entero']??1));
    if($enter<1||intval($enter)==0){
        echo ('No has introducido un numero entero positivo');
    }else{
        $entero=$enter;
        $factorial=1;

        if($enter == 0 || $enter == 1){
            $factorial = 1;
            echo ('El factorial de '.$entero.' es '.$factorial.'<br /><br />');
    }
        while($enter>1){
            $factorial=$enter*$factorial;
            $enter-=1;
    }
        echo ('El factorial de '.$entero.' es '.$factorial.'(Hecho con while)<br /><br />');

         $enter=$entero;
        $factorial=1;

        for($cont=$enter;$cont>1;$cont--){
            $factorial=$cont*$factorial;
    }
        echo ('El factorial de '.$entero.' es '.$factorial.'(Hecho con for)<br /><br />');

        $enter=$entero;
        $factorial=1;

        do{
            $factorial=$factorial*$enter;
            $enter--;
        }
        while($enter>1);
        echo ('El factorial de '.$entero.' es '.$factorial.'(Hecho con do-While)<br /><br />');
    }
?>
  